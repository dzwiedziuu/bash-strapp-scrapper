#!/bin/bash
#$1 - subdirectory
#$2 - link

subdirectory=$1
if [ ! -e "$subdirectory" ]; then
	mkdir $subdirectory
fi
authCookie=`cat authorization.txt`
file=`date +%Y%m%d`.json
if [ ! -e "$subdirectory/$file" ]; then
	responseCode=`curl --cookie "$authCookie" --header "Accept: application/json"  --output response  --insecure --silent --write-out '%{http_code}' $2`
	if ! [ "$responseCode" == "200" ]; then
		echo "Problem: with getting image of $personId: HTTP_CODE=$responseCode"
	else
		cp response $subdirectory/$file
		echo "List stored under $subdirectory/$file"
	fi
else
	echo "Last list already exists in $subdirectory/$file"
fi
if [ -e response ]; then
	rm response
fi
