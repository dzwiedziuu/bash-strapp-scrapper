#!/bin/bash
#export LANG=pl_PL.UTF-8

tsIdx=1
if [ "$1" = "test" ]; then
	filesToProcess="20181119.json 20191007.json" 
	filesToProcessReverted="20191007.json 20181119.json"
else
	filesToProcess=`ls data` 
	filesToProcessReverted=`ls data | tac`
fi
for file in $filesToProcess 
do
	echo "Processing file: " $file
	timestamp=`echo $file | cut -c1-8`
	timestamps[$tsIdx]=$timestamp
	tsIdx=$((tsIdx+1))
	
	personIdx=1
	for personId in `./jq-win32.exe ".[].id" ./data/$file | sed -e "s/\r//g"`
	do
		personIdByIdx[$personIdx]=$personId
		personLastTimestamp[$personId]=$timestamp
		personIdx=$((personIdx+1))
	done
	
	personIdx=1
	for firstName in `./jq-win32.exe ".[].firstName" ./data/$file | sed 's/ /_/g'`
	do
		personId=${personIdByIdx[$personIdx]}
		personFirstName[$personId]=$firstName
		personIdx=$((personIdx+1))
	done
	
	personIdx=1
	for lastName in `./jq-win32.exe ".[].lastName" ./data/$file | sed 's/ /_/g'`
	do
		personId=${personIdByIdx[$personIdx]}
		#echo $personIdx $personId $lastName
		personLastName[$personId]=$lastName
		personIdx=$((personIdx+1))
	done
	
	personIdx=1
	for grade in `./jq-win32.exe ".[].grade" ./data/$file | sed 's/ /_/g'`
	do
		personId=${personIdByIdx[$personIdx]}
		if [ ! "$grade" = "null" ]; then
			personGrade[$personId]=$grade
		fi
		personIdx=$((personIdx+1))
	done
	
	personIdx=1
	for location in `./jq-win32.exe ".[].location" ./data/$file | sed 's/ /_/g'`
	do
		personId=${personIdByIdx[$personIdx]}
		if [ ! "$location" = "null" ]; then
			personLocation[$personId]=$location
		fi
		personIdx=$((personIdx+1))
	done
	
	personIdx=1
	for startDate in `./jq-win32.exe ".[].startDate" ./data/$file | sed 's/ /_/g'`
	do
		personId=${personIdByIdx[$personIdx]}
		if [ ! "$startDate" = "null" ]; then
			personStartDate[$personId]=$startDate
		fi
		personIdx=$((personIdx+1))
	done
done

for file in $filesToProcessReverted #descending order 
do
	echo "Processing file: " $file
	timestamp=`echo $file | cut -c1-8`
	
	for personId in "${!personLastTimestamp[@]}"
	do
		found[$personId]=0
	done
	
	personIdx=1
	for personId in `./jq-win32.exe ".[].id" ./data/$file | sed -e "s/\r//g"`
	do
		found[$personId]=1
		personIdx=$((personIdx+1))
	done
	for personId in "${!found[@]}"
	do
		if [ ${found[$personId]} -eq 0 ]; then
			personNotFoundFirstTimestamp[$personId]=$timestamp
		fi
	done
done

touch tmp
for personId in "${!personNotFoundFirstTimestamp[@]}"
do
	if [[ "${personLastTimestamp[$personId]}" < "${personNotFoundFirstTimestamp[$personId]}" ]]; then
		# TBD for new people
		# if [ "${personStartDate[$personId]}" = "" ]; then
		#	echo "missing"
		#	lastDetailFile=`ls detail/$personId/* | tail -1`
		#	echo $lastDetailFile
		#fi
		daysInSollers=$(( (`echo ${personNotFoundFirstTimestamp[$personId]} | tr -d '"' | { read endDateStr ; date --date $endDateStr '+%s'; }` - `echo ${personStartDate[$personId]} | tr -d '"' | { read startDateStr ; date --date $startDateStr '+%s'; }`) / 86400 ))
		echo ${personLastTimestamp[$personId]}-${personNotFoundFirstTimestamp[$personId]} ${personFirstName[$personId]} ${personLastName[$personId]} "(ID: $personId, grade: ${personGrade[$personId]}, location: ${personLocation[$personId]}, startDate: ${personStartDate[$personId]}, daysInSollers: $daysInSollers)" >> tmp
	fi
done
cat tmp | sort | awk '{printf("%5d : %s\n", NR,$0)}' > result.txt
rm tmp

cat result.txt