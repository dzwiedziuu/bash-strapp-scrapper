#!/bin/bash
listDir=search
if [ ! -e "$listDir" ]; then
	mkdir $listDir
fi
authCookie=`cat authorization.txt`
file=`date +%Y%m%d`.json
echo '{"filters": [], "searchText": ""}' > content
if [ ! -e "$listDir/$file" ]; then
	responseCode=`curl --request POST --cookie "$authCookie" --header "Accept: application/json" --header "Content-Type: application/json;charset=UTF-8" --data @content  --output tmp  --insecure --silent --write-out '%{http_code}' 'https://strapp.sollers.eu/api/sollers/search?page=0&paginationOption=1000&paginationOptionLabel=1000&sortParameters=SOLLERS_FIRST_NAME,ASC'`
	if ! [ "$responseCode" == "200" ]; then
		echo "Problem: with getting image of $personId: HTTP_CODE=$responseCode"
		exit 1
	else
		cp tmp $listDir/$file
		echo "Last list of sollers stored under $listDir/$file"
	fi
else
	echo "Last list of sollers already exists in $listDir/$file"
fi
rm tmp
rm content


