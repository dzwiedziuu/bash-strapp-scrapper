#!/bin/bash
#$1 - subdirectory
#$2 - link before $personId
#$3 - link after $personId

subdirectory=$1
linkBeforePersonId=$2
linkAfterPersonId=$3
if [ ! -e "$subdirectory" ]; then
	mkdir $subdirectory
fi
authCookie=`cat authorization.txt`
file=`ls data | tail -1`
for personId in `./jq-win32.exe ".[].id" ./data/$file | sed -e "s/\r//g"`
do
	if [ ! -e "$subdirectory/$personId.jpg" ]; then
		responseCode=`curl --cookie "$authCookie" --header "Accept: application/json" --output response --insecure --silent --write-out '%{http_code}' $linkBeforePersonId$personId$linkAfterPersonId`
		if ! [ "$responseCode" == "200" ]; then
			echo "Problem: with getting response of $personId: HTTP_CODE=$responseCode"
		else
			cp response $subdirectory/$personId.jpg
			echo "Data of $personId stored under $subdirectory/$personId.jpg"
		fi
	else
		echo "Data of $personId already exists in $subdirectory/$personId.jpg"
	fi
done
if [ -e response ]; then
	rm response
fi
